# Secret Santa Email System

## Introduction

This project automates the process of sending Secret Santa emails for family Christmas events. It randomly assigns each family member a person to gift and notifies them via email.

## Features

- Email Automation: The script automates the process of sending emails.
- Random Assignment: Participants are randomly assigned a person to gift.
- Progress Tracking: The script displays the progress of sending emails.
- Error Handling: In case of an error, the script logs the issue.

## Usage

### Requirements

Ensure you have the following installed:

- [Fry v3.4.0^](https://fatscript.org/en/general/setup.html): the FatScript interpreter.

### Setting Up

1. Copy `list_model.csv` to `list.csv` and update it with the names and emails of all participants.
2. Copy `template_model.txt` to `template.txt` and customize it with your desired email message.
2. Copy `email_config_model.fat` to `email_config.fat` and set it with your email configurations.

### Running the Script

Execute `run_me.fat` to start the process. The script will:

- Read participant details from `list.csv`.
- Shuffle the list to randomly assign Secret Santa pairs.
- Generate personalized emails using `template.txt`.
- Send emails to each participant with their assigned Secret Santa.

### Note

- This script is designed for a specific use case. Modify the script if you need to adapt it for different scenarios.

## Tutorial Video

Follow along with the [tutorial video](https://www.youtube.com/watch?v=Y1GnFhpUEQM).

## Contributing

If you have suggestions for new features or improvements, please open an issue on the [Secret Santa GitLab](https://gitlab.com/aprates/secret-santa/issues).

### Donations

If you find Secret Santa useful and would like to support its development, you can [Buy me a coffee](https://www.buymeacoffee.com/aprates).

## Licensing

[MIT License](LICENSE) © 2023-2024 Antonio Prates.
